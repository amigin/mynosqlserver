using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using DotNetCoreDecorators;
using MyNoSqlServer.Api.Hubs;
using MyNoSqlServer.Domains.Db;
using MyNoSqlServer.Domains.Db.Partitions;
using MyNoSqlServer.Domains.Db.Rows;
using MyNoSqlServer.Domains.Db.Tables;
using MyNoSqlServer.Tcp.TcpContracts;
using MyTcpSockets;

namespace MyNoSqlServer.Api
{
    public class ChangesTcpService : TcpContext<IMyNoSqlTcpContract>
    {
        private static readonly Dictionary<string, Dictionary<long,ChangesTcpService>> Subscribers 
            = new Dictionary<string, Dictionary<long,ChangesTcpService>>();
        
        public static readonly MyServerTcpSocket<IMyNoSqlTcpContract> TcpServer = 
            new MyServerTcpSocket<IMyNoSqlTcpContract>(new IPEndPoint(IPAddress.Any, 5125))
            .RegisterSerializer(()=> MyNoSqlTcpSerializer.Instance)
            .SetService(()=>new ChangesTcpService())
            .AddLog(Console.WriteLine);

        private string _tableSubscribed;
        
        protected override Task OnConnectAsync()
        {
            return Task.CompletedTask;
        }

        protected override Task OnDisconnectAsync()
        {
            if (_tableSubscribed == null) 
                return Task.CompletedTask;
            
            lock (Subscribers)
            {
                if (!Subscribers.ContainsKey(_tableSubscribed))
                    return Task.CompletedTask;

                if (Subscribers[_tableSubscribed].ContainsKey(Id))
                    Subscribers[_tableSubscribed].Remove(Id);
            }

            return Task.CompletedTask;
        }
        
        private Task HandleGreetingAsync(GreetingContract greetingContract)
        {
            Console.WriteLine("Greeting: "+greetingContract.Name);
            SetContextName(greetingContract.Name);
            return Task.CompletedTask;
        }

        private static IReadOnlyList<ChangesTcpService> GetConnectionsToBroadcast(string tableName)
        {
            lock (Subscribers)
            {
                if (Subscribers.ContainsKey(tableName))
                    return null;
                
                if (Subscribers[tableName].Count == 0)
                    return null;

                return Subscribers[tableName].Values.ToList();
            }            
        }
        
        public static void BroadcastInitTable(DbTable dbTable)
        {

            var connections = GetConnectionsToBroadcast(dbTable.Name);
            
            if (connections == null)
                return;
            
            var packetToBroadcast = new InitTableContract
            {
                TableName = dbTable.Name,
                Data = dbTable.GetAllRecords(null).ToHubUpdateContract()
            };

            foreach (var connection in connections)
                connection.SendPacketAsync(packetToBroadcast);

        }
        
        public static void BroadcastInitPartition(DbTable dbTable, DbPartition partition)
        {

            var connections = GetConnectionsToBroadcast(dbTable.Name);
            
            if (connections == null)
                return;
            
            var packetToBroadcast = new InitPartitionContract
            {
                TableName = dbTable.Name,
                PartitionKey = partition.PartitionKey,
                Data = partition.GetAllRows().ToHubUpdateContract()
            };

            foreach (var connection in connections)
                connection.SendPacketAsync(packetToBroadcast);

        }
        
        public static void BroadcastRowsUpdate(DbTable dbTable, IReadOnlyList<DbRow> entities)
        {

            var connections = GetConnectionsToBroadcast(dbTable.Name);
            
            if (connections == null)
                return;
            
            var packetToBroadcast = new UpdateRowsContract
            {
                TableName = dbTable.Name,
                Data = entities.ToHubUpdateContract()
            };

            foreach (var connection in connections)
                connection.SendPacketAsync(packetToBroadcast);
        }

        public static void BroadcastRowsDelete(DbTable dbTable, IReadOnlyList<DbRow> dbRows)
        {
            var connections = GetConnectionsToBroadcast(dbTable.Name);
            
            if (connections == null)
                return;
            
            var packetToBroadcast = new DeleteRowsContract
            {
                TableName = dbTable.Name,
                RowsToDelete = dbRows.Select(row => (row.PartitionKey, row.RowKey)).AsReadOnlyList()
            };
            
            foreach (var connection in connections)
                connection.SendPacketAsync(packetToBroadcast);
        }

        protected override Task HandleIncomingDataAsync(IMyNoSqlTcpContract data)
        {
            switch (data)
            {
                case PingContract _:
                    return SendPacketAsync(PongContract.Instance);
                
                case GreetingContract greetingContract:
                    return HandleGreetingAsync(greetingContract);
                
                case SubscribeContract subscribeContract:
                    return HandleSubscribeAsync(subscribeContract);
                
            }
            
            return Task.CompletedTask;
        }

        private Task HandleSubscribeAsync(SubscribeContract subscribeContract)
        {
            if (string.IsNullOrEmpty(subscribeContract.TableName))
                return Task.CompletedTask;

            var table = DbInstance.GetTable(subscribeContract.TableName);

            if (table == null)
                return Task.CompletedTask;

            _tableSubscribed = subscribeContract.TableName;


            lock (Subscribers)
            {
                if (!Subscribers.ContainsKey(_tableSubscribed))
                    Subscribers.Add(_tableSubscribed, new Dictionary<long, ChangesTcpService>());
                
                Subscribers[_tableSubscribed].Add(Id, this);
            }
            
            var rows = table.GetAllRecords(null);

            var initContract = new InitTableContract
            {
                TableName = subscribeContract.TableName,
                Data = rows.ToHubUpdateContract() 
            };

            return SendPacketAsync(initContract);
        }
        
    }
}