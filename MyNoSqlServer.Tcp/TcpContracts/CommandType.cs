﻿namespace MyNoSqlServer.Tcp.TcpContracts
{
    public enum CommandType
    {
        Ping, Pong, Greeting, Subscribe, InitTable, InitPartition, UpdateRows, DeleteRow
    }
}